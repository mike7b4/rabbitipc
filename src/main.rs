use std::path::PathBuf;
use std::os::unix::net::{UnixStream, UnixListener};
use std::io::prelude::*;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Sender, Receiver};
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;
use std::thread;
// External crates...
use url::Url;
use structopt::StructOpt;
use serde::{Deserialize, Serialize};
use json;
const MAX_IPC_WORKERS: usize = 4;
mod rabbit {
    pub enum Error {
        SSLError,
        ConnectionTimeout,
    }
}
// Kind of ugly Same event handler for all
//
enum Event {
    // generic error that should be printed in main thread.
    Error(rabbit::Error),
    Command(json::JsonValue),
    Payload(json::JsonValue),
    Terminated,
    // Rabbit
    Connected,
    Disconnected,
    // Rabbit Acked a message where usize is the id
    Acked(usize),
    // Main loop may send this to a thread
    Terminate,
    // Main -> Rabbit thread
    Connect,
    Disconnect,
}

struct IpcClient {
    stream: UnixStream,
    pub addr: std::os::unix::net::SocketAddr,
    event: Sender<Event>,
}

#[derive(Serialize)]
struct IpcResult {
    result: String,
}

impl IpcResult {
    fn new(result: String) -> Self {
        Self { result }
    }
}


impl IpcClient {
    fn new(stream: UnixStream, addr: std::os::unix::net::SocketAddr, event: Sender<Event>) -> Self {
        Self { stream,
               addr,
               event,
        }
    }

    fn handle_incoming(&mut self, data: String) -> bool {

        let js = match json::parse(&data) {
            Ok(js) => { js }
            Err(err) => {
                eprintln!("Invalid JSON '{}': '{}'", data, err);
                self.stream.write_all(&format!("Invalid JSON '{}': '{}'", data, err).as_bytes()).unwrap_or(());
                return false;
            }
        };
        let respond = IpcResult::new("ok".to_string());
        println!("{}", serde_json::to_string(&respond).unwrap_or(String::new()));
        self.stream.write_all(&serde_json::to_string(&respond).unwrap_or(String::new()).as_bytes()).unwrap_or(());

        if js.has_key("command") {
            self.event.send(Event::Command(js)).unwrap_or_else(|err| { eprintln!("{}", err); });
        } else {
            self.event.send(Event::Payload(js)).unwrap_or_else(|err| { eprintln!("{}", err); });
        }
        return true;
    }

    fn run(&mut self, id: usize, terminate: Arc<AtomicBool>) {
        let mut timeout = 3000;
        println!("{} IPC client got a slot", id);
        while !terminate.load(Ordering::Relaxed) && timeout >= 0 {
            let mut data = String::new();
            let size = match self.stream.read_to_string(&mut data) {
                Ok(size) => {
                    if size > 0 {
                        // reset timer
                        timeout = 3000;
                        if self.handle_incoming(data) == false {
                            terminate.store(true, Ordering::Relaxed);
                            continue;
                        }
                    }
                    size
                }
                Err(err) => {
                    eprintln!("{}", err);
                    break;
                }
            };

            if size == 0 {
                thread::sleep(Duration::from_millis(500));
                timeout -= 500;
                println!("alive {}", timeout);
            }
        }
        println!("{} IPC client exiting...", id);
        self.event.send(Event::Terminated).unwrap_or_else(|err| { eprintln!("{}", err); });
    }
}

struct IpcServer {
    handle: UnixListener,
    name: PathBuf,
    workers: Vec<Worker>,
    sender: Sender<IpcClient>,
}

// Ipc Server creates MAX_IPC_WORKERS
impl IpcServer {
    fn new(name: PathBuf) -> Self {
        let mut workers = Vec::with_capacity(MAX_IPC_WORKERS);
        let (sender, receiver) = channel();
        let receiver = Arc::new(Mutex::new(receiver));
        for id in 0..MAX_IPC_WORKERS {
            workers.push(Worker::new(id, receiver.clone()));
        }

        let handle = UnixListener::bind(&name).expect("Could not create socket");
        handle.set_nonblocking(true).expect("Could not set it non blocking");
        Self {
            handle,
            name,
            workers,
            sender,
        }
    }

    fn accept(&self, signal: Sender<Event>) -> bool {
        match self.handle.accept() {
            Ok((stream, addr)) => {
                println!("New client {:?}", addr);
                let client = IpcClient::new(stream, addr, signal);
                // feed the worker
                self.sender.send(client).unwrap_or(());
                true
            }
            Err(_) => { false }
        }
    }

    fn terminate(&mut self) {
        for worker in &mut self.workers {
            worker.terminate();
        }
    }
}

impl Drop for IpcServer {
    fn drop(&mut self) {
        // Ignore possible error.
        std::fs::remove_file(&self.name).unwrap_or_else(|_|{
            println!("{:?} already removed?", self.name);
        });
    }
}

struct RabbitClient {
}

impl RabbitClient {
    fn new() -> Self {
        Self {}
    }
}

struct Worker {
    id: usize,
    // We need this option because handle may go out of scope when join
    handle: Option<thread::JoinHandle<()>>,
    terminate: Arc<AtomicBool>,
}

// This spawn a new worker thread that will be idle until
// it receives a IPC to handle and then block until client terminates.
impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<Receiver<IpcClient>>>) -> Self {
        let terminate = Arc::new(AtomicBool::new(false));
        let terminate2 = terminate.clone();
        Self {
            id,
            handle: Some(thread::spawn(move|| {
                let mut elapsed = 0;
                while !terminate2.load(Ordering::Relaxed) {
                    let client = receiver.lock().unwrap().try_recv();
                    if let Ok(mut client) = client {
                        client.run(id, terminate2.clone());
                    }
                    thread::sleep(Duration::from_millis(500));
                    elapsed += 1;
                    if elapsed % 20 == 0 {
                        println!("{} is running", id);
                    }
                }
            })),
            terminate: terminate,
        }
    }

    fn terminate(&mut self) {
        self.terminate.store(true, Ordering::Relaxed);
        if let Some(handle) = self.handle.take() {
            handle.join().unwrap();
        }
    }
}

#[derive(Deserialize, Serialize, StructOpt)]
struct Args {
    #[structopt(short, long, default_value = "amqp://localhost:4040")]
    /// TODO use Url but not sure how to serialize:/
    server: String, 
    #[structopt(short, long, default_value = "")]
    exchange: String,
    #[structopt(short = "C", long, default_value = "")]
    ca_cert: PathBuf,
    #[structopt(short, long, default_value = "/etc/ssl/system.pem")]
    cert_key: PathBuf,
}

fn main() {
    let args = Args::from_args();
    let url = match Url::parse(&args.server) {
        Ok(url) => { url }
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(-1);
        }
    };

    let terminate = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGQUIT, terminate.clone()).unwrap();
    signal_hook::flag::register(signal_hook::SIGTERM, terminate.clone()).unwrap();
    signal_hook::flag::register(signal_hook::SIGINT, terminate.clone()).unwrap();

    let mut ipc_server = IpcServer::new(PathBuf::from("/var/run/rabbitipc.sock"));

    let (rabbit_tx, rabbit_rx) = channel();
    let (ipc_signal_tx, ipc_signal_rx) = channel();
    let rabbit_thread_handle = std::thread::spawn(move || {
        let _rabbit = RabbitClient::new();
        loop {
            match rabbit_rx.try_recv() {
                Ok(_event) => { break; }
                Err(_) => {}
            };
            std::thread::sleep(Duration::from_millis(500));
        }
        println!("Rabbit terminated");
    });

    let mut elapsed = 0;
    let mut client_connections = 0;
    while !terminate.load(Ordering::Relaxed) {
        if ipc_server.accept(ipc_signal_tx.clone()) {
            client_connections += 1;
            if client_connections == 0 {
                client_connections += 1;
            }
            println!("A client connected");
        }

        match ipc_signal_rx.try_recv() {
            Ok(send) => {
                match send {
                    Event::Terminated => {
                        client_connections -= 1;
                    }
                    Event::Payload(data) => {
                        println!("{}", data);
                    }
                    Event::Command(js) => {
                        println!("{}", js["command"]);
                    }
                     _ => {}
                }
            }
            Err(_) => {}
        }
        std::thread::sleep(std::time::Duration::from_millis(500));
        elapsed += 1;
        if elapsed % 20 == 0 {
            if client_connections >= 0 {
                println!("{}s There are {} clients \"active\".", elapsed/2, client_connections);    
                client_connections -= 1;
            }
        }
        
    }

    ipc_server.terminate();

    rabbit_tx.send(Event::Terminate).unwrap_or(());
    rabbit_thread_handle.join().unwrap_or(());
    println!("{:?}", url.host());
}
